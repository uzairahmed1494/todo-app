const API_URL = process.env.REACT_APP_API_ENDPOINT;

// tasks APIs
export const GET_TASKS = `${API_URL}/tasks`;
export const CREATE_TASK = `${API_URL}/task/create`;
export const GET_TASK = `${API_URL}/task`;
export const UPDATE_TASK = `${API_URL}/task/update`;
export const DELETE_TASK = `${API_URL}/task/delete`;
