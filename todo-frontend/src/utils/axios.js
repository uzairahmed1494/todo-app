import axios from "axios";
import { toast } from "react-toastify";

/**
 * @description Call an asynchronous XHR request
 * @param {String} url* required
 * @param {String} method* required
 * @param {Object} data optional
 * @param {Object} customHeader optional
 * @return {Object} XHR response
 */
export const request = async ({
  url,
  method,
  data,
  params,
  customHeaders,
  ...rest
}) => {
  if (!url) {
    throw new Error("request func arg @param url is missing");
  }
  if (!method) {
    throw new Error(`
      request func arg @param method is missing. Valid options "GET"|"POST"|"PUT" etc
    `);
  }

  // header object based on variant if "customHeaders" object provided
  const headers = { ...customHeaders };
  const result = await axios(url, {
    method,
    headers,
    ...(data && { data }),
    ...(params && { params }),
    ...rest,
  });
  return result.data;
};

export const setupInterceptors = ({ debug = false }) => {
  /**
   * @description echo() prints a message if allowed, of a certain type of console variant
   * @param {*} show {Boolean}
   * @param {*} type {String} Can be log, warn, debug
   * @param {*} message {String}
   */
  const echo = ({ show, type, message }) => {
    if (!show) {
      return;
    }
    /* eslint-disable no-console */
    console.group("🤯🤯 @@@AXIOS INTERCEPTOR@@@ 🤯🤯");
    console[type](message);
    console.groupEnd("@@@AXIOS INTERCEPTOR@@@");
    /* eslint-enable */
  };

  axios.interceptors.response.use(
    (response) => {
      echo({ show: debug, type: "dir", message: response });
      return response;
    },
    async (error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        const err =
          error.response?.data?.message ||
          "Something went wrong. Please contact administrator";
        toast.error(err);
      } else if (error.request) {
        // Request made, but no response was received
        echo({ show: debug, type: "dir", message: error.request });
      } else {
        echo({
          show: debug,
          type: "warn",
          message: `Something occurred setting the request that set off an error ${error.message}`,
        });
      }
      return Promise.reject(error);
    }
  );
};
