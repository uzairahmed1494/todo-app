import * as actionTypes from "../actions/actionType/actionTypes";

const initialState = {
  data: [],
  detail: undefined,
  message: "",
  updated: false,
};

export default function tasks(state = initialState, action) {
  switch (action.type) {
    // get task list
    case actionTypes.GET_TASKS_RESPONSE: {
      return {
        ...state,
        data: [...action.data.tasks],
      };
    }
    // get task detail
    case actionTypes.GET_TASK_RESPONSE: {
      return {
        ...state,
        detail: action.data.task,
      };
    }
    // create and update and clear task
    case actionTypes.CREATE_TASK_RESPONSE:
    case actionTypes.UPDATE_TASK_RESPONSE:
    case actionTypes.CLEAR_FORM: {
      return {
        ...state,
        message: action?.data?.message,
      };
    }
    // delete task
    case actionTypes.DELETE_TASK_RESPONSE: {
      return {
        ...state,
        updated: !state.updated,
      };
    }
    default:
      return state;
  }
}
