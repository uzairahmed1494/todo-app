import { toast } from "react-toastify";
import {
  GET_TASK,
  GET_TASKS,
  CREATE_TASK,
  UPDATE_TASK,
  DELETE_TASK,
} from "../../config/urlConstant";
import { request } from "../../utils/axios";
import * as actionTypes from "./actionType/actionTypes";

// dispatch data
const dispatchData = (data, type) => {
  return {
    type,
    data,
  };
};

// get list of tasks
export const getTasks = () => async (dispatch) => {
  try {
    const data = await request({
      url: GET_TASKS,
      method: "get",
    });
    dispatch(dispatchData(data, actionTypes.GET_TASKS_RESPONSE));
  } catch (err) {
    toast.error(err?.response?.data?.message || "something went wrong!");
  }
};

// get one task/detail data by specific id
export const getTaskDetail = (id) => async (dispatch) => {
  try {
    const data = await request({
      url: `${GET_TASK}/${id}`,
      method: "get",
    });
    dispatch(dispatchData(data, actionTypes.GET_TASK_RESPONSE));
  } catch (err) {
    toast.error(err?.response?.data?.message || "something went wrong!");
  }
};

// create new task
export const createTask = (data) => async (dispatch) => {
  try {
    const resp = await request({
      url: CREATE_TASK,
      method: "post",
      data,
    });
    toast.success(resp?.data?.message || "Resource created!");
    dispatch(dispatchData(resp, actionTypes.CREATE_TASK_RESPONSE));
  } catch (err) {
    toast.error(err?.response?.data?.message || "something went wrong!");
  }
};

// update specific/one task
export const updateTask = (data) => async (dispatch) => {
  try {
    const resp = await request({
      url: `${UPDATE_TASK}/${data?.id}`,
      method: "put",
      data,
    });
    toast.success(resp?.data?.message || "Updated successfully!");
    dispatch(dispatchData(resp, actionTypes.UPDATE_TASK_RESPONSE));
  } catch (err) {
    toast.error(err?.response?.data?.message || "something went wrong!");
  }
};

// delete specific/one task
export const deleteTask = (id) => async (dispatch) => {
  try {
    const resp = await request({
      url: `${DELETE_TASK}/${id}`,
      method: "delete",
    });
    toast.success(resp?.data?.message || "Deleted successfully!");
    dispatch(dispatchData(resp, actionTypes.DELETE_TASK_RESPONSE));
  } catch (err) {
    toast.error(err?.response?.data?.message || "something went wrong!");
  }
};

// clear all messages
export const clearForm = () => (dispatch) => {
  dispatch(dispatchData({ message: "" }, actionTypes.CLEAR_FORM));
};

export const getTasksReq = () => getTasks();
export const getTaskDetailReq = (id) => getTaskDetail(id);
export const createTaskReq = (data) => createTask(data);
export const updateTaskReq = (data) => updateTask(data);
export const deleteTaskReq = (id) => deleteTask(id);
export const clearFormReq = () => clearForm();
