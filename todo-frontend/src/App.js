import { React } from "react";
import "./App.scss";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import TaskList from "./routes/TaskList";
import Task from "./routes/Task";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="container">
      <h1 className="heading">Todo App</h1>
      <Router>
        <Switch>
          <Route exact path="/" component={TaskList} />
          <Route path="/tasks/:id" component={Task} />
        </Switch>
      </Router>
      {/* a toaster conatiner */}
      <ToastContainer />
    </div>
  );
}

export default App;
