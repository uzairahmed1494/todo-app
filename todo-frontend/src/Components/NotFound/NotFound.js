import React from "react";

const NotFound = () => {
  return <div className="highlight_text">No Item Found</div>;
};

export default NotFound;
