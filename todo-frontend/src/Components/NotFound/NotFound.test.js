import React from "react";
import { render, screen } from "@testing-library/react";
import NotFound from "./NotFound";

describe("test cases for NotFound component", () => {
  test("renders NotFound childrens", () => {
    render(<NotFound />);
    const linkElement = screen.getByText(/No Item Found/i);
    expect(linkElement).toBeInTheDocument();
  });
});
