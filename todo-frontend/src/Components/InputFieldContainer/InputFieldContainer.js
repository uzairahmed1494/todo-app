import React from "react";
import propTypes from "prop-types";

const InputFieldContainer = ({ children, label }) => {
  return (
    <div className="row">
      <div className="col-25">
        <label htmlFor={label}>{label}</label>
      </div>
      <div className="col-75">{children}</div>
    </div>
  );
};

InputFieldContainer.propTypes = {
  children: propTypes.any,
  label: propTypes.string,
};
export default InputFieldContainer;
