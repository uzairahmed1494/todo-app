import React from "react";
import { render, screen } from "@testing-library/react";
import InputFieldContainer from "./InputFieldContainer";

const props = {
  children: <div />,
  label: "Title",
};

describe("test cases for InputFieldContainer component", () => {
  test("renders InputFieldContainer childrens", () => {
    render(<InputFieldContainer {...props} />);
    const linkElement = screen.getByText(/Title/i);
    expect(linkElement).toBeInTheDocument();
  });
});
