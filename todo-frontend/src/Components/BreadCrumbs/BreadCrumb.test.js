import React from "react";
import { render, screen, within } from "@testing-library/react";
import BreadCrumb from "./BreadCrumb";

const props = {
  activePage: "create",
};

describe("test cases for BreadCrumb component", () => {
  test("renders BreadCrumb", () => {
    render(<BreadCrumb {...props} />);
    const linkElement = screen.getByText(/Tasks/i);
    expect(linkElement).toBeInTheDocument();
  });

  it("should render list of 2 items", () => {
    render(<BreadCrumb {...props} />);
    const list = screen.getByRole("list");
    const { getAllByRole } = within(list);
    const items = getAllByRole("listitem");
    expect(items.length).toBe(2);
  });
});
