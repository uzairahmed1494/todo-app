import React from "react";
import propTypes from "prop-types";
import { useHistory } from "react-router";

const BreadCrumb = ({ activePage }) => {
  const history = useHistory();
  return (
    <ul className="breadcrumb">
      <li onClick={() => history.push("/")}>
        <span>Tasks</span>
      </li>
      <li>
        <span className="highlight_text">{activePage}</span>
      </li>
    </ul>
  );
};

BreadCrumb.propTypes = {
  activePage: propTypes.any,
};

export default BreadCrumb;
