import React from "react";
import { render } from "@testing-library/react";
import TaskList from "./TaskList";

const props = {
  data: [
    { id: 1, title: "test-1", description: "testing description-1" },
    { id: 2, title: "test-1", description: "testing description-2" },
  ],
  deleteTaskCall: jest.fn(),
};

describe("test cases for list component", () => {
  it("should render list of 2 items", () => {
    const { container } = render(<TaskList {...props} />);
    const list = container.querySelectorAll(".table tbody tr");
    expect(list.length).toBe(2);
  });
});
