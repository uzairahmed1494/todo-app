import React, { useEffect } from "react";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteTaskReq, getTasksReq } from "../../redux/actions/tasks";
import TaskList from "./TaskList";

const TaskListContainer = ({ getTasksCall, data, deleteTaskCall, updated }) => {
  // get task list
  useEffect(() => {
    getTasksCall();
  }, [updated]);

  return <TaskList data={data} deleteTaskCall={deleteTaskCall} />;
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getTasksCall: () => getTasksReq(),
      deleteTaskCall: (id) => deleteTaskReq(id),
    },
    dispatch
  );
};

const mapStateToProps = ({ tasks: { data, updated } }) => ({
  data,
  updated,
});

TaskListContainer.propTypes = {
  getTasksCall: propTypes.func,
  data: propTypes.array,
  deleteTaskCall: propTypes.func,
  updated: propTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskListContainer);
