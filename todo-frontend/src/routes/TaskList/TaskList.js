import React from "react";
import propTypes from "prop-types";
import { useHistory } from "react-router";
import NotFound from "../../Components/NotFound";

const TaskList = ({ data, deleteTaskCall }) => {
  const history = useHistory();

  // display limited title or description values
  const displayValue = (val, index) => {
    if (val?.length > index) {
      return `${val.substring(0, index)}...`;
    }
    return val;
  };

  return (
    <>
      <button
        type="button"
        className="create-btn"
        onClick={() => history.push("tasks/create")}
      >
        create
      </button>
      {data?.length === 0 && <NotFound />}
      {data?.length > 0 && (
        <table className="table">
          <thead>
            <tr>
              <th>Title</th>
              <th>Description</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data?.map((item) => (
              <tr key={item.id}>
                <td>{displayValue(item?.title, 20)}</td>
                <td>{displayValue(item?.description, 40)}</td>
                <td>
                  <button
                    type="button"
                    className="icon"
                    onClick={() => history.push(`/tasks/${item.id}`)}
                  >
                    <i className="fa fa-edit" />
                  </button>
                </td>
                <td>
                  <button
                    type="button"
                    className="icon"
                    onClick={() => deleteTaskCall(item.id)}
                  >
                    <i className="fa fa-trash-o" />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}
    </>
  );
};

TaskList.propTypes = {
  deleteTaskCall: propTypes.func,
  data: propTypes.array,
};

export default TaskList;
