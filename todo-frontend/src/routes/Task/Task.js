import React, { useEffect, useState } from "react";
import propTypes from "prop-types";
import BreadCrumb from "../../Components/BreadCrumbs";
import InputFieldContainer from "../../Components/InputFieldContainer";

const Task = ({ postSubmit, updateDetail, id, detail, message, clearForm }) => {
  const [fieldValues, setFieldValues] = useState({});
  const [error, setError] = useState("");

  // set task detail
  useEffect(() => {
    if (detail && id !== "create") {
      setFieldValues({ ...detail });
    }
  }, [detail, id]);

  // clear or reset form data
  useEffect(() => {
    if (message === "Resource created") {
      setFieldValues({});
      clearForm();
    }
  }, [message]);

  // validate fields
  const validateFields = () => {
    const obj = {};
    if (!fieldValues?.title) {
      obj.titleErr = "Please fill the title field";
    }

    if (!fieldValues?.description) {
      obj.descriptionErr = "Please fill the description field";
    }

    setError(obj);
    return Object.keys(obj).length === 0 ? "" : obj;
  };

  // create or update form data
  const submitForm = () => {
    if (validateFields() === "") {
      if (id === "create") {
        postSubmit(fieldValues);
      } else {
        updateDetail(fieldValues);
      }
    }
  };

  return (
    <>
      <BreadCrumb activePage={id !== "create" ? fieldValues?.title : id} />
      <div className="form-container">
        <InputFieldContainer label="Title">
          <input
            className={error?.titleErr ? "errorInputField" : "inputField"}
            type="text"
            id="title"
            name="tile"
            placeholder="Enter Your Title"
            value={fieldValues?.title || ""}
            onChange={(e) => {
              setFieldValues((prevState) => ({
                ...prevState,
                title: e.target.value,
              }));
            }}
          />
          <span className="errorField">{error?.titleErr}</span>
        </InputFieldContainer>
        <InputFieldContainer label="Description">
          <input
            className={error?.descriptionErr ? "errorInputField" : "inputField"}
            type="text"
            id="description"
            name="description"
            placeholder="Enter Your Description"
            value={fieldValues?.description || ""}
            onChange={(e) => {
              setFieldValues((prevState) => ({
                ...prevState,
                description: e.target.value,
              }));
            }}
          />
          <span className="errorField">{error?.descriptionErr}</span>
        </InputFieldContainer>
        <div className="row">
          <button type="button" className="create-btn" onClick={submitForm}>
            {id === "create" ? "create" : "update"}
          </button>
        </div>
      </div>
    </>
  );
};

Task.propTypes = {
  postSubmit: propTypes.func,
  updateDetail: propTypes.func,
  id: propTypes.any,
  detail: propTypes.any,
  message: propTypes.string,
  clearForm: propTypes.func,
};

export default Task;
