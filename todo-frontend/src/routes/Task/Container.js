import React, { useEffect } from "react";
import propTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  createTaskReq,
  getTaskDetailReq,
  getTasksReq,
  updateTaskReq,
  clearFormReq,
} from "../../redux/actions/tasks";
import Task from "./Task";

const TaskContainer = ({
  createTaskCall,
  updateTaskCall,
  detail,
  match: {
    params: { id },
  },
  getTaskDetailCall,
  message,
  clearForm,
}) => {
  // get task detail
  useEffect(() => {
    if (id !== "create") {
      getTaskDetailCall(id);
    }
  }, [id]);

  return (
    <Task
      postSubmit={(values) => createTaskCall(values)}
      updateDetail={(values) => updateTaskCall(values)}
      id={id}
      detail={detail}
      message={message}
      clearForm={() => clearForm()}
    />
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getTasksCall: () => getTasksReq(),
      createTaskCall: (values) => createTaskReq(values),
      getTaskDetailCall: (id) => getTaskDetailReq(id),
      updateTaskCall: (values) => updateTaskReq(values),
      clearForm: () => clearFormReq(),
    },
    dispatch
  );
};

const mapStateToProps = ({ tasks: { detail, message } }) => ({
  detail,
  message,
});

TaskContainer.propTypes = {
  createTaskCall: propTypes.func,
  updateTaskCall: propTypes.func,
  detail: propTypes.any,
  match: propTypes.any,
  getTaskDetailCall: propTypes.func,
  message: propTypes.string,
  clearForm: propTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskContainer);
