import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Task from "./Task";

const props = {
  postSubmit: jest.fn(),
  updateDetail: jest.fn(),
  message: "",
  updated: false,
};

describe("test cases for createWarehouse component", () => {
  test("fill and submit the form in create case", async () => {
    const { container } = render(
      <Task {...props} id="create" detail={undefined} />
    );

    const payload = {
      title: "test-1",
      description: "test-purpose",
    };
    const inputs = container.querySelectorAll(".form-container input");

    const titleField = inputs[0];
    const descriptionField = inputs[1];

    // first check if inputs have empty values
    expect(titleField).toHaveValue("");
    expect(descriptionField).toHaveValue("");

    fireEvent.change(titleField, { target: { value: payload.name } });
    fireEvent.change(descriptionField, { target: { value: payload.address } });

    const submitButton = screen.getByRole("button", { name: /create/i });
    fireEvent.click(submitButton);
  });

  test("fill and submit the form in update case", async () => {
    const item = { id: 1, title: "test-1", description: "testing-purpose" };
    const itemId = 1;
    const { container } = render(<Task {...props} id={itemId} detail={item} />);
    const newData = {
      title: "test-1",
      description: "test-1",
    };
    const inputs = container.querySelectorAll(".form-container input");

    const titleField = inputs[0];
    const descriptionField = inputs[1];

    // first check if inputs have pre-filled values
    expect(titleField).toHaveValue(item.title);
    expect(descriptionField).toHaveValue(item.description);

    fireEvent.change(titleField, { target: { value: newData.title } });
    fireEvent.change(descriptionField, {
      target: { value: newData.description },
    });

    const submitButton = screen.getByRole("button", { name: /update/i });
    fireEvent.click(submitButton);
  });
});
