"use strict";

const app = require("express")();
const { v4: uuidv4 } = require("uuid");
const cors = require("cors");
const tasksContainer = require("./tasks.json");
const bodyParser = require("body-parser");

app.use(cors());
app.use(bodyParser.json());
/**
 * GET /tasks
 *
 * Return the list of tasks with status code 200.
 */
app.get("/tasks", (req, res) => {
  return res.status(200).json(tasksContainer);
});

/**
 * Get /task/:id
 *
 * id: unique string
 *
 * Return the task for the given id.
 *
 * If found return status code 200 and the resource.
 * If not found return status code 404.
 * If id is not valid return status code 400.
 */
app.get("/task/:id", (req, res) => {
  const id = req.params.id;

  const task = tasksContainer.tasks.find((item) => item.id === id);

  if (task) {
    return res.status(200).json({
      task,
    });
  } else {
    return res.status(404).json({
      message: "Not found.",
    });
  }
});

/**
 * PUT /task/update/:id/:title/:description
 *
 * id: unique string
 * title: string
 * description: string
 *
 * Update the task with the given id.
 * If the task is found and update as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not valid  return a status code 400.
 */
app.put("/task/update/:id", (req, res) => {
  const id = req.params.id;

  const task = tasksContainer.tasks.find((item) => item.id === id);

  if (task) {
    task.title = req.body.title;
    task.description = req.body.description;
    return res.status(204).json({
      message: "Resource created",
    });
  } else {
    return res.status(404).json({
      message: "Not found",
    });
  }
});

/**
 * POST /task/create/:title/:description
 *
 * title: string
 * description: string
 *
 * Add a new task to the array tasksContainer.tasks with the given title and description.
 * Return status code 201.
 */
app.post("/task/create", (req, res) => {
  const task = {
    id: uuidv4(),
    title: req.body.title,
    description: req.body.description,
  };

  tasksContainer.tasks.push(task);

  return res.status(201).json({
    message: "Resource created",
  });
});

/**
 * DELETE /task/delete/:id
 *
 * id: unique string
 *
 * Delete the task linked to the  given id.
 * If the task is found and deleted as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not valid return a status code 400.
 */
app.delete("/task/delete/:id", (req, res) => {
  const id = req.params.id;

  const taskIndex = tasksContainer.tasks.findIndex((item) => item.id === id);

  if (taskIndex !== -1) {
    tasksContainer.tasks.splice(taskIndex, 1);

    return res.status(200).json({
      message: "Deleted successfully",
    });
  } else {
    return res.status(404).json({
      message: "Not found",
    });
  }
});

app.listen(9001, () => {
  process.stdout.write("the server is available on http://localhost:9001/\n");
});
