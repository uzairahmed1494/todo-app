# README #

This README would normally document whatever steps are necessary to get your application up and running.

### For BackEnd ###

* Goto todo-backend folder.
* Run the following command:
*   npm i
*   node serve.js

### For FrontEnd ###

* Goto todo-frontend folder.
* Run the following commands:
*   npm i
*   npm start

### Tools and libraries that used ###

* prop-types for data types verification.
* react-toastify ( A simple react notification ).
* react^17 and redux.
* eslint.
* Modular CSS (scss).
* react-testing-library (for unit testing).